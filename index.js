'use strict';
// https://github.com/ArtemOnigiri/RayCasting2D
// https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-sphere-intersection

const cnv = document.getElementById('cnv');
const ctx = cnv.getContext('2d');
const w = cnv.width = window.innerWidth;
const h = cnv.height = window.innerHeight;

const circles = [
	{x: w / 2, y: - 100 + h / 2, ra: 100, r: 0.9, g: 0.7, b: 0.4},
	{x: w / 2 + 300, y: h / 2, ra: 100, r: 0.3, g: 0.7, b: 0.4}
];

ctx.lineWidth = 4;

let vec2 = (x, y) => ({x, y});
let add = (a, b) => ({x: a.x + b.x, y: a.y + b.y});
let sub = (a, b) => ({x: a.x - b.x, y: a.y - b.y});
let mulS = (a, b) => ({x: a.x * b, y: a.y * b});
let dot = (a, b) => a.x * b.x + a.y * b.y;
let length = a => Math.sqrt(a.x * a.x + a.y * a.y);
let norm = a => ({x: a.x / length(a), y: a.y / length(a)});

function drawCircles() {
	for (let i = 0; i < circles.length; i++) {
		ctx.fillStyle = 'rgba(' + ~~(circles[i].r * 255) + ', ' + ~~(circles[i].g * 255) + ', ' + ~~(circles[i].b * 255) + ', 1)';
		ctx.beginPath();
		ctx.arc(circles[i].x, circles[i].y, circles[i].ra, 0, Math.PI * 2, false);
		ctx.fill();
	}
}

class Camera {
    constructor() {
        this.cameraPos = vec2(225, h / 2);
    }

    startScene() {
        ctx.fillStyle = '#ccc';
        ctx.fillRect(0, 0, w, h);
        ctrl.drawCamera();
        drawCircles();
    }

    /**
     * @param {vec2} ro - ray origin
     * @param {vec2} rd - ray direction
     * @param {number} ra - radius of sphere
     * @param {vec2} ce - center of sphere
     */
    sphIntersect(ro, rd, ra, ce) {
        // vecFromCameraToCircle
        const os = sub(ce, ro);

        const normRayDirec = norm(sub(rd, ro));

        const proj = dot(os, normRayDirec);
        if (proj <= 0) return vec2(-1.0, -1.0);

        const catet2 = dot(os, os) - Math.pow(proj, 2);
        
        if (catet2 < 0) return vec2(-1.0, -1.0);

        this.drawCatet(ce, add(ro, mulS(normRayDirec, proj)));

        const catet = Math.sqrt(catet2);
        if (catet > ra) return vec2(-1.0, -1.0);

        const h = Math.sqrt(ra*ra - catet*catet);

        // return distance
        return vec2(proj - h, proj + h);
    }

    drawCamera() {
        ctx.strokeStyle = '#000';
        ctx.strokeRect(100, h / 2 - 25, 75, 50);
        ctx.beginPath();
        ctx.moveTo(175, h / 2 - 25);
        ctx.lineTo(175, h / 2 + 25);
        ctx.lineTo(225, h / 2 + 45);
        ctx.lineTo(225, h / 2 - 45);
        ctx.lineTo(175, h / 2 - 25);
        ctx.closePath();
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(165, h / 2 - 50, 25, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(120, h / 2 - 45, 20, 0, 2 * Math.PI);
        ctx.stroke();
    }
    
    drawRay(point) {
        ctx.strokeStyle = '#fff';
        ctx.beginPath();
        ctx.moveTo(this.cameraPos.x, this.cameraPos.y);
        ctx.lineTo(point.x, point.y);
        ctx.stroke();
    }

    drawCatet(ce, vecProj) {
        ctx.strokeStyle = '#28203b';
        ctx.beginPath();
        ctx.moveTo(ce.x, ce.y);
        ctx.lineTo(vecProj.x, vecProj.y);
        ctx.stroke();
    }

    drawIntersection(distance, ro, rd) {
        if(distance.x < 0.0) return;

        const normRayDir = norm(sub(rd, ro));

        const tg0 = add(ro, mulS(normRayDir, distance.x));
        const tg1 = add(ro, mulS(normRayDir, distance.y));

        ctx.fillStyle = '#f00';
        ctx.beginPath();
        ctx.arc(tg0.x, tg0.y, 10, 0, 2 * Math.PI);
        ctx.fill();
        ctx.fillStyle = '#070';
        ctx.beginPath();
        ctx.arc(tg1.x, tg1.y, 10, 0, 2 * Math.PI);
        ctx.fill();
    }

    rayCast(point) {
        const tg = add(this.cameraPos, mulS(sub(point, this.cameraPos), 9999));
        this.drawRay(tg);

        for (let i = 0; i < circles.length; i++) {
            const sp = vec2(circles[i].x, circles[i].y);
            const distances = this.sphIntersect(this.cameraPos, point, circles[i].ra, sp);

            ctx.strokeStyle = '#9d9ea1';
            ctx.beginPath();
            ctx.moveTo(this.cameraPos.x, this.cameraPos.y);
            ctx.lineTo(sp.x, sp.y);
            ctx.stroke();

            this.drawIntersection(distances, this.cameraPos, point);
        }
    }
}

const ctrl = new Camera();
ctrl.startScene();

function onMouseUpdate(e) {
    ctrl.startScene();
    ctrl.rayCast(vec2(e.pageX, e.pageY))
}

document.addEventListener('mousemove', throttle(onMouseUpdate, 5), false);










function throttle(func, ms) {
    return function(args) {
        let previousCall = this.lastCall;
        this.lastCall = Date.now();

        if (previousCall === undefined || (this.lastCall - previousCall) > ms) {
            func(args)
        }
    }
}